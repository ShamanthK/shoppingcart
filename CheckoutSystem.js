//Step 1: Checkout function to calculate the price of the fruits without the offer
var actualPrice = (fruits) =>
{
var totalPrice = 0;

for(var i=0; i<fruits.length; i++){
  
  if(fruits[i] == 'Apple'){
    totalPrice = totalPrice + 0.6;
  }
  else if(fruits[i] == 'Orange'){
    totalPrice = totalPrice + 0.25
  }
}
  //print the total cost of the cart
   console.log(totalPrice.toFixed(2)+"p");    
}


// Step 2: Code to calculate the price of the cart after the offers have been applied
var apples = [];
var oranges = [];


//Checkout function to calculate the total cost after the offers have been applied
var discountPrice = (fruits) =>
{  
  for(var i=0; i<fruits.length; i++){
    
    if(fruits[i] == 'Apple'){
      
      apples.push(fruits[i]);
      
    }
    else{
      oranges.push(fruits[i]);
    }
  }
  
  var m = apples.length;
  var n = oranges.length;

  if(m%2 == 0){
    
    //Calculate the total cost of Apples with the offer when the number of Apples is even and print it
    appleCost = (m/2)*0.6;
    console.log(appleCost+"p");
  }
  
  else{
    
    //Calculate cost of Apples with the offer when the number of apples bought is odd and print it
    appleCost = ((Math.floor(m/2))+1)*0.6;
    console.log(appleCost.toFixed(2)+"p");
  }
  
  if(n%3 == 0){
    
    //Calculate the total cost of Oranages with the offer when the number of Oranges is a multiple of 3 and print it
    orangeCost = (n/3)*2*0.25;
    console.log(orangeCost+"p");
  }
  
  else{
    
    
   //Calculate the total cost of Oranages with the offer when the number of Oranges is not a multiple of 3 and print it
    orangeCost = (n-Math.floor(n/3))*0.25;
    console.log(orangeCost+"p");
  }
 
  //Calculate the total cost of the Apples and Oranges in the cart with the offer and print it
  var totalDiscountCost = appleCost + orangeCost;
  console.log(totalDiscountCost+"p");
}
  
//Array taking the list of items in the cart as a String  
var fruitsArray = ['Orange', 'Apple', 'Orange', 'Orange', 'Apple', 'Apple', 'Orange', 'Orange', 'Orange', 'Apple', 'Orange', 'Orange', 'Orange'];

//call checkout functions to calculate the cost of the cart without offer and with offer.
actualPrice(fruitsArray);
discountPrice(fruitsArray);
